Overview
========
Version 3 show expands the example with a control form that allows a user to create new subjects and observers.
The form encapsulates data that it needs to manage the display, namely to show a list of existing observers and
balls.


Changes
=======
- Expand the control form to show the current observers and balls, and to show the observed balls the selected
observed.

- Added a form for create subjects and observers.

- Added the ability to create balls

- Removed the initial creation of balls and observers from the main program.

- Simplified construction of a ball by having it assign it's own Id from a circular sequence

- Modified the assignment of default values for the ball so the radius would never be zero or bigger than what
could fit the box.  Also, the default starting position now takes into account the radius.

- Removed Ball's notification counter, since it no longer has any value because Version 2 decouple the subject
and observer in time.

