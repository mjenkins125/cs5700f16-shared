﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace BouncingBall
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Box.Width = 600;
            Box.Height = 400;
            Box.Label = "My Box";

            // TODO: For now, just create a balls, start it moving, and display it's position
            Ball b = new Ball() { Radius = 10, X = 1, Y = 1, Direction = 67, Speed = 4, TimeUnit = 10 };
            b.Start();

            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine($"X={b.X}, Y={b.Y}");
                Thread.Sleep(100);
            }

            Console.WriteLine("Press Enter to Exit");
            Console.ReadLine();

        }
    }
}
