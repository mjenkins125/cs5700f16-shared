﻿using System;
using System.Drawing;

using Shapes;

namespace BouncingBall
{
    public partial class GraphicalDisplay : ShapeObserver
    {
        public GraphicalDisplay()
        {
            InitializeComponent();
        }

        public Box MyBox { get; set; }

        protected override void RefreshDisplay()
        {
            if (IsDisposed) return;

            Graphics graphics = boxPanel.CreateGraphics();
            graphics.Clear(Color.White);

            foreach (Shape shape in BallsBeingObserved)
            {
                shape.Draw(graphics);
            }
        }

        private void GraphicalDisplay_Load(object sender, EventArgs e)
        {
            Text = Title;
            StartRefreshTimer();
            if (MyBox != null)
                boxPanel.Size = MyBox.BoxSize;
        }
    }
}
