﻿using System;
using System.Linq;

namespace Triangles
{
    public class Triangle
    {
        public double[] SideLengths { get; set; }

        public double ComputeArea()
        {
            double halfOfPerimeter = SideLengths.Sum()/2;

            return Math.Sqrt(halfOfPerimeter *
                                    (halfOfPerimeter - SideLengths[0])*
                                    (halfOfPerimeter - SideLengths[1])*
                                    (halfOfPerimeter - SideLengths[2]));
        }
    }
}
